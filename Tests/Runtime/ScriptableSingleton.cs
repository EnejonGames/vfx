using UnityEngine;
#if UNITY_EDITOR
using System.IO;
using UnityEditor;
#endif

namespace Enejon.Tests.Vfx
{

    public interface IScriptableSingleton
    {
        void OnLoad();
        void Update();
    }
    
    public abstract class ScriptableSingleton<T> : ScriptableObject, IScriptableSingleton where T : ScriptableObject, IScriptableSingleton
    {
        private static T _instance;
        public static T Instance => _instance = _instance ?? Load();
        
        private static T Load()
        {
            _instance = Resources.Load<T>(Path);
#if UNITY_EDITOR
            if (!_instance)
            {
                _instance = CreateInstance<T>();
                string s = $"Assets/Resources/{Path}.asset";
                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(s));
                AssetDatabase.CreateAsset(_instance, s);
            }
#endif
            _instance.OnLoad();
            
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying) return _instance;
#endif
            GameObject go = new GameObject();
            go.name = $"{typeof(T).Name}_Updater";
            go.AddComponent<ScriptableSingletonUpdater>().singleton = _instance;
            
            return _instance;
        }

        public virtual void Update() {}

        public virtual void OnLoad() {}

        private static string Path => $"Singletons/{typeof(T).ToString().Replace('.', '/')}";

    }
}