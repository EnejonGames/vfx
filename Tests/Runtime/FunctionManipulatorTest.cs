﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Enejon.Vfx.Functions;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public class FunctionManipulatorTest : BaseFunctionTest<FunctionManipulator>
    {
        private float _valueMultiplier;
        private float _timeMultiplier;
        private AnimationCurve curve;
        

        [SetUp]
        public void Setup()
        {
            _valueMultiplier = 1;
            _timeMultiplier = 1;
        }

        [UnityTest]
        public IEnumerator FunctionManipulatorFlatOneReturnsFunction()
        {
            curve = VfxTestHandler.Instance.flatOne;

            SetExpectedPercentages(0, 0.5f, 1);
            yield return AssertCase();
        }
        
        [UnityTest]
        public IEnumerator FunctionManipulatorFlatZeroReturnsZero()
        {
            curve = VfxTestHandler.Instance.flatZero;

            SetExpectedPercentages(0, 0, 0);
            yield return AssertCase();
        }
        
        [UnityTest]
        public IEnumerator FunctionManipulatorLinearCurveReturnsCombination()
        {
            curve = VfxTestHandler.Instance.linearCurve;
            
            SetExpectedPercentages(0, 0.25f, 1);
            yield return AssertCase();
        }
        
        [UnityTest]
        public IEnumerator FunctionManipulatorValueMultiplierReturnsFunctionMultiplied()
        {
            _valueMultiplier = 2;
            curve = VfxTestHandler.Instance.flatOne;
            
            SetExpectedPercentages(0,1,2);
            yield return AssertCase();
        }
        
        [UnityTest]
        public IEnumerator FunctionManipulatorTimeMultiplierWorksAsIntended()
        {
            _timeMultiplier = .5f;
            curve = VfxTestHandler.Instance.parabolaCurve;
            
            SetExpectedPercentages(0, TestConstants.PAROBALA_QUARTER_VALUE * .5f, 1);
            
            yield return AssertCase();
        }

        private void SetExpectedPercentages(float zeroPercent, float fiftyPercent, float hundredPercent)
        {
            _expectedZeroPercent = zeroPercent;
            _expectedFiftyPercent = fiftyPercent;
            _expectedHundredPercent = hundredPercent;
        }

        protected override IEnumerator StartRoutine()
        {
            FunctionCurve functionCurve = CreateBasicFunctionCurve();
            _instance.animationCurve = curve;
            _instance.timeMultiplier = _timeMultiplier;
            _instance.valueMultiplier = _valueMultiplier;
            _instance.function = functionCurve;
            yield return null;
        }


    }
}