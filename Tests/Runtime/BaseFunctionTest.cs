﻿using System.Collections;
using Enejon.Vfx.Functions;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public abstract class BaseFunctionTest<T> : BaseMonoBehaviourTest<T>
        where T : Function
    {
        protected float _expectedZeroPercent;
        protected float _expectedFiftyPercent;
        protected float _expectedHundredPercent;

        protected virtual IEnumerator AssertCase()
        {
            yield return StartRoutine();
            AssertFloat(_expectedZeroPercent, _instance.Calculate(TestConstants.ZERO_PERCENT));
            AssertFloat(_expectedFiftyPercent, _instance.Calculate(TestConstants.FIFTY_PERCENT));
            AssertFloat(_expectedHundredPercent, _instance.Calculate(TestConstants.HUNDRED_PERCENT));
        }

        protected abstract IEnumerator StartRoutine();
    }
}