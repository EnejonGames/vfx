using UnityEngine;

namespace Enejon.Tests.Vfx
{
    public class ScriptableSingletonUpdater : MonoBehaviour
    {

        public IScriptableSingleton singleton;

        private void Update()
        {
            singleton.Update();
        }
    }
}