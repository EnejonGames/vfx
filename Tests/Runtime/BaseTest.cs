﻿using NUnit.Framework;

namespace Enejon.Tests.Vfx
{
    public abstract class BaseTest
    {
        protected void AssertFloat(float expected, float actual)
        {
            Assert.AreEqual(expected, actual, TestConstants.FLOAT_DELTA);
        }
    }
}