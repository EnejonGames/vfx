﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Enejon.Vfx;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public class LineTest : BaseLineTest<Line>
    {
        [UnityTest]
        public IEnumerator BasicBaseLinePositionsAreCorrect()
        {

            _instance.minimumPoints = DEFAULT_MINIMUM_POINTS;
            var positions = _instance.GetBasePositions();
            Assert.Contains(_start.transform.position, positions);
            Assert.Contains(_end.transform.position, positions);
            yield return null;
        }
        

    }
}