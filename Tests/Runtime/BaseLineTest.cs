﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public abstract class BaseLineTest<T>: BaseMonoBehaviourTest<T> where T : Line
    {
        protected const int DEFAULT_MINIMUM_POINTS = 3;
        
        protected GameObject _start;
        protected GameObject _end;
        protected float Distance => Vector3.Distance(_start.transform.position, _end.transform.position);

        [SetUp]
        public void Setup()
        {
            _start = new GameObject("startPosition");
            _end = new GameObject("endPosition");
            SetupPositions(new Vector3(1,0,0), new Vector3(-1,0,0));
        }

        protected void SetupPositions(Vector3 start, Vector3 end)
        {
            _start.transform.position = start;
            _end.transform.position = end;
            _instance.startPoint = _start.transform;
            _instance.endPoint = _end.transform;
        }
        
        [UnityTest]
        public IEnumerator BaseLineScalesWithDistance()
        {
            SetupPositions(new Vector3(10,0,0), new Vector3(-10,0,0));
            _instance.minimumPoints = DEFAULT_MINIMUM_POINTS;
            _instance.density = 1;
            yield return null;
            Assert.Greater(_instance.GetBasePositions().Length, DEFAULT_MINIMUM_POINTS);
        }
        
        [UnityTest]
        public IEnumerator HigherScaleValueScalesMore()
        {
            SetupPositions(new Vector3(10,0,0), new Vector3(-10,0,0));
            _instance.minimumPoints = DEFAULT_MINIMUM_POINTS;
            _instance.density = 1;
            yield return null;
            int smallScaleResult = _instance.GetBasePositions().Length;
            _instance.density = 2;
            yield return null;
            int bigScaleResult = _instance.GetBasePositions().Length;
            Assert.Greater(bigScaleResult, smallScaleResult);
        }
    }
}