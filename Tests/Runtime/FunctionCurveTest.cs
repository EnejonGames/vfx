﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using Enejon.Vfx.Functions;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public class FunctionCurveTest : BaseFunctionTest<FunctionCurve>
    {
        private float _valueMultiplier;
        private float _timeMultiplier;
        private AnimationCurve _curve;
        
       


        [UnityTest]
        public IEnumerator TestLinearCurve()
        {
            _valueMultiplier = 1;
            _timeMultiplier = 1;
            _curve = VfxTestHandler.Instance.linearCurve;
            
            _expectedZeroPercent = 0;
            _expectedFiftyPercent = 0.5f;
            _expectedHundredPercent = 1;
            yield return AssertCase();
        }
        
        [UnityTest]
        public IEnumerator TestParabolaCurve()
        {
            _valueMultiplier = 1;
            _timeMultiplier = 1;
            _curve = VfxTestHandler.Instance.parabolaCurve;
            
            _expectedZeroPercent = 0;
            _expectedFiftyPercent = 1;
            _expectedHundredPercent = 0;
            yield return AssertCase();
        }
        
        [UnityTest]
        public IEnumerator TestValueMultiplier()
        {
            _valueMultiplier = 2;
            _timeMultiplier = 1;
            _curve = VfxTestHandler.Instance.linearCurve;

            _expectedZeroPercent = 0;
            _expectedFiftyPercent = 1;
            _expectedHundredPercent = 2;
            yield return AssertCase();
        }
        
        [UnityTest]
        public IEnumerator TestTimeMultiplier()
        {
            _valueMultiplier = 1;
            _timeMultiplier = .5f;
            _curve = VfxTestHandler.Instance.parabolaCurve;

            _expectedZeroPercent = 0;
            _expectedFiftyPercent = TestConstants.PAROBALA_QUARTER_VALUE;
            _expectedHundredPercent = 1;
            yield return AssertCase();
        }

        protected override IEnumerator StartRoutine()
        {
            _instance.animationCurve = _curve;
            _instance.timeMultiplier = _timeMultiplier;
            _instance.valueMultiplier = _valueMultiplier;
            yield return null;
        }
    }
}