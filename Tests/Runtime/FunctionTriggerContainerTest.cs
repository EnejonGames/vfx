﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine.TestTools;
using Enejon.Vfx;
using Moq;
using Enejon.Vfx.Functions;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public class FunctionTriggerContainerTest : BaseMonoBehaviourTest<FunctionTriggerContainer>
    {
        private Mock<FunctionTrigger> _triggerMock;
        private FunctionCurve _curve;
        
        [SetUp]
        public void Setup()
        {
            _triggerMock = new Mock<FunctionTrigger>();
            _instance.triggerables = new List<FunctionTrigger>() {_triggerMock.Object};
            _curve = CreateBasicFunctionCurve();
            _curve.subscriber = new List<FunctionTriggerContainer>() {_instance};
        }

        [UnityTest]
        public IEnumerator TriggersWithGreaterThan()
        {
            
            _instance.comparison = FunctionTriggerContainer.Comparison.GreaterThanResult;
            _instance.triggerValue = 1.1f;

            _curve.Calculate(1f);
            _triggerMock.Verify(t => t.Trigger());
            yield return null;
        }
        
        [UnityTest]
        public IEnumerator TriggersWithLessThan()
        {
            _instance.comparison = FunctionTriggerContainer.Comparison.LessThanResult;
            _instance.triggerValue = .9f;

            _curve.Calculate(1f);
            _triggerMock.Verify(t => t.Trigger());
            yield return null;
        }
        
        [UnityTest]
        public IEnumerator OnlyTriggersOnceWhenNotUntriggered()
        {
            
            _instance.comparison = FunctionTriggerContainer.Comparison.GreaterThanResult;
            _instance.triggerValue = 1.1f;

            _curve.Calculate(1f);
            _curve.Calculate(1f);
            _triggerMock.Verify(t => t.Trigger(), Times.Once());
            yield return null;
        }
        
        [UnityTest]
        public IEnumerator TriggersMultipleTimesWhenUntriggered()
        {
            
            _instance.comparison = FunctionTriggerContainer.Comparison.LessThanResult;
            _instance.triggerValue = .5f;

            _curve.Calculate(1f);
            _curve.Calculate(0);
            _curve.Calculate(1f);
            _triggerMock.Verify(t => t.Trigger(), Times.Exactly(2));
            yield return null;
        }
        
    }
}