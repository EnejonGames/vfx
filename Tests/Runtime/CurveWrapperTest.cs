﻿using System;
using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.TestTools;
using Enejon.Vfx;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public class CurveWrapperTest : BaseTest
    {
        private const string LINEAR = "linear";
        private const string PARABOLA = "bicubic";
        
        [TestCase(0, 0.5f, 1, 
            1, 1, LINEAR)]
        [TestCase(0, 1, 0, 
            1, 1, PARABOLA)]
        [TestCase(0, 1, 2, 
            2, 1, LINEAR)]
        [TestCase(0, TestConstants.PAROBALA_QUARTER_VALUE, 1, 
            1, .5f, PARABOLA)]
        public void TestLinearCurve(float expectedZeroPercent, float expectedFiftyPercent, float expectedHundredPercent, 
            float valueMultiplier, float timeMultiplier, string curveType)
        {
            AnimationCurve animationCurve;
            switch (curveType)
            {
                case (LINEAR):
                    animationCurve = VfxTestHandler.Instance.linearCurve;
                    break;
                case (PARABOLA):
                    animationCurve = VfxTestHandler.Instance.parabolaCurve;
                    break;
                default:
                    throw new ArgumentException();
            }

            var curveWrapper = new CurveWrapper()
            {
                Curve = animationCurve,
                TimeMultiplier = timeMultiplier,
                ValueMultiplier = valueMultiplier
            };
            float zeroPercent = curveWrapper.Calculate(0f);
            float fiftyPercent = curveWrapper.Calculate(.5f);
            float hundredPercent = curveWrapper.Calculate(1f);
            AssertFloat(expectedZeroPercent, zeroPercent);
            AssertFloat(expectedFiftyPercent, fiftyPercent);
            AssertFloat(expectedHundredPercent, hundredPercent);
        }

    }
}