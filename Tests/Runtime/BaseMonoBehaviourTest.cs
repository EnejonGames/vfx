﻿using NUnit.Framework;
using UnityEngine;
using Enejon.Vfx.Functions;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public abstract class BaseMonoBehaviourTest<T>: BaseTest where T: MonoBehaviour
    {
        protected T _instance;
        protected GameObject _gameObject;
        
        [SetUp]
        protected void Setup()
        {
            _gameObject = new GameObject("gameObject");
            _instance = _gameObject.AddComponent<T>();
        }
        
        protected FunctionCurve CreateBasicFunctionCurve()
        {
            FunctionCurve functionCurve = _gameObject.AddComponent<FunctionCurve>();
            functionCurve.animationCurve = VfxTestHandler.Instance.linearCurve;
            functionCurve.timeMultiplier = 1;
            functionCurve.valueMultiplier = 1;
            return functionCurve;
        }
    }
}