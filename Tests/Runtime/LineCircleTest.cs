﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public class LineCircleTest : BaseLineTest<LineCircle>
    {
        [UnityTest]
        public IEnumerator SmallCircleContainsMinimumPoints()
        {
            _instance.minimumPoints = DEFAULT_MINIMUM_POINTS;
            Assert.AreEqual(DEFAULT_MINIMUM_POINTS, _instance.GetBasePositions().Length);
            yield return null;
        }
        
        [UnityTest]
        public IEnumerator AllPointsHaveSameDistanceToCenter()
        {
            _instance.minimumPoints = 1;
            _instance.density = 1;
            for (int i = 0; i < 10; i++)
            {
                _start.transform.position = new Vector3(i,0,0);
                List<Vector3> points = _instance.GetBasePositions().ToList();
                points.ForEach(p => 
                    AssertFloat(Distance, Vector3.Distance(_start.transform.position, p)));
            }
            yield return null;
        }
        
        [UnityTest]
        public IEnumerator AllPointsHaveSameDistanceToNeighbors()
        {
            _instance.minimumPoints = 1;
            _instance.density = 1;
            for (int i = 0; i < 10; i++)
            {
                _start.transform.position = new Vector3(i,0,0);
                var points = _instance.GetBasePositions();
                if (points.Length >= 2)
                {
                    float neighborDistance = Vector3.Distance(points[0], points[1]);
                    for (int j = 0; j < points.Length; j++)
                    {
                        if (j != 0) AssertFloat(neighborDistance, Vector3.Distance(points[j - 1], points[j]));
                        if (j != points.Length - 1) AssertFloat(neighborDistance, Vector3.Distance(points[j + 1], points[j]));
                    }
                }
            }
            yield return null;
        }
    }
}