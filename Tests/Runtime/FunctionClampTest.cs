﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Enejon.Vfx;
using Enejon.Vfx.Functions;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public class FunctionClampTest : BaseFunctionTest<FunctionClamp>
    {
        private List<FunctionClamp.ClampValue> _clampValues;
        private float _range;
        [UnityTest]
        public IEnumerator EmptyFunctionClampDoesNothing()
        {
            _expectedZeroPercent = 0;
            _expectedFiftyPercent = .5f;
            _expectedHundredPercent = 1;
            _clampValues = new List<FunctionClamp.ClampValue>();
            _range = 0f;
            
            yield return AssertCase();
        }

        [UnityTest]
        public IEnumerator ClampingZeroWorks()
        {
            _expectedZeroPercent = 1;
            _expectedFiftyPercent = .5f;
            _expectedHundredPercent = 1;
            _range = .1f;
            _clampValues = new List<FunctionClamp.ClampValue>()
            {
                new FunctionClamp.ClampValue()
                {
                    time = 0, value = 1
                }
            };
            yield return AssertCase();
        }
        
        [UnityTest]
        public IEnumerator ClampingZeroAndOneWorks()
        {
            _expectedZeroPercent = 1;
            _expectedFiftyPercent = .5f;
            _expectedHundredPercent = 0;
            _range = .1f;
            _clampValues = new List<FunctionClamp.ClampValue>() {
                new FunctionClamp.ClampValue()
                {
                    time = 0, value = 1
                },
                new FunctionClamp.ClampValue()
                {
                    time = 1, value = 0
                }                    
            };
            yield return AssertCase();
        }

        [UnityTest]
        public IEnumerator RangeBehavesAsIntended()
        {
            _expectedZeroPercent = 2;
            _expectedFiftyPercent = 1.25f;
            _expectedHundredPercent = 1;
            _range = 1;
            _clampValues = new List<FunctionClamp.ClampValue>() {
                new FunctionClamp.ClampValue()
                {
                    time = 0, value = 2
                },
            };
            yield return AssertCase();
        }
        
        protected override IEnumerator StartRoutine()
        {
            FunctionCurve functionCurve = CreateBasicFunctionCurve();
            _instance.function = functionCurve;
            _instance.clampValues = _clampValues;
            _instance.range = _range;
            yield return null;
        }

    }
}