﻿using UnityEditor;
using UnityEngine;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public class VfxTestHandler : ScriptableSingleton<VfxTestHandler>
    {
        public AnimationCurve linearCurve;
        public AnimationCurve parabolaCurve;
        public AnimationCurve flatOne;
        public AnimationCurve flatZero;
    }
}