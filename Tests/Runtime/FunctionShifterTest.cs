﻿using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;
using Enejon.Vfx;
using Enejon.Vfx.Functions;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Tests.Vfx
{
    public class FunctionShifterTest : BaseFunctionTest<FunctionShifter>
    {

        [UnityTest]
        public IEnumerator NoShiftingCalculatesBaseCurve()
        {
            _expectedZeroPercent = 0;
            _expectedFiftyPercent = .5f;
            _expectedHundredPercent = 1;
            return AssertCase();
        }
        [UnityTest]
        public IEnumerator ShiftingCalculatesDifferentCurve()
        {
           
            _instance.minimumDelta = .1f;
            _instance.maximumDelta = .1f;
            _instance.Trigger();

            _expectedZeroPercent = .1f;
            _expectedFiftyPercent = .6f;
            _expectedHundredPercent = 1; // FunctionCurve clamped, so 1 does not change
            return AssertCase();
        }
        [UnityTest]
        public IEnumerator ShiftingMoreThanMaxValueLoops()
        {
            _instance.minimumDelta = 1.2f;
            _instance.maximumDelta = 1.2f;
            _instance.maximumValue = 1;
            _instance.Trigger();

            _expectedZeroPercent = .2f;
            _expectedFiftyPercent = .7f;
            _expectedHundredPercent = 1; // FunctionCurve clamped, so 1 does not change
            return AssertCase();
        }

        [UnityTest]
        public IEnumerator ShiftingDoesNotLeaveRange()
        {
            _instance.minimumDelta = 0;
            _instance.maximumDelta = .01f;
            _instance.maximumValue = 2;
            float currentValue = 0;
            yield return StartRoutine();
            for (int i = 0; i < 100; i++)
            {
                _instance.Trigger();
                float result = _instance.Calculate(0);
                float stepSize = result - currentValue;
                Assert.LessOrEqual(stepSize, _instance.maximumDelta);
                currentValue += result;
            }
        }

        protected override IEnumerator StartRoutine()
        {
            var functionCurve = CreateBasicFunctionCurve();
            _instance.function = functionCurve;
            yield return null;
        }
    }
}