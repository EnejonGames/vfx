﻿namespace Enejon.Tests.Vfx
{
    public static class TestConstants
    {
        public const float FLOAT_DELTA = 0.0001f;
        public const float PAROBALA_QUARTER_VALUE = 0.647577107f;
        public const float ZERO_PERCENT = 0;
        public const float FIFTY_PERCENT = .5f;
        public const float HUNDRED_PERCENT = 1;
    }
}