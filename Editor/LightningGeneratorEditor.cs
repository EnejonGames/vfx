﻿using System.Collections.Generic;
using Enejon.Vfx;
using UnityEditor;
using UnityEngine;

namespace Enejon.Vfx.LineRendererManipulation.Editor
{
#if UNITY_EDITOR
    [CustomEditor(typeof(LightningGenerator))]
    public class LightningGeneratorEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            LightningGenerator instance = (LightningGenerator)target;
            if(GUILayout.Button("Dissolve")) 
            {
                instance.TriggerDissolve();
            }            
            
        }
    }
#endif
}