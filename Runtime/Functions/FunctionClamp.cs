using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Enejon.Vfx.Functions
{
    public class FunctionClamp : Function
    {
        public Function function;        
        public List<ClampValue> clampValues = new List<ClampValue>();
        public float range = 0.1f;
        protected override float DoCalculate(float parameter) 
        {
            float result = function.Calculate(parameter);
            var rangeQuery = clampValues.Where(cv => parameter > cv.time - range && parameter < cv.time + range).ToList();            
            if(rangeQuery.Count == 0) return result;

            ClampValue clampInRange = rangeQuery.First();
            float weight = (range - Mathf.Abs(parameter-clampInRange.time)) / range;
            float weightedResult = (clampInRange.value * weight) + (result * (1-weight));
            return weightedResult;
        }
        [System.Serializable]
        public struct ClampValue 
        {
            public float time;
            public float value;
        }
    }    
}