using System;
using UnityEngine;
using System.Linq;
using Enejon.Vfx.LineRendererManipulation;

namespace Enejon.Vfx.Functions
{
    public class FunctionCurve : Function
    {
        public AnimationCurve animationCurve;
        public float valueMultiplier = 1;
        public float timeMultiplier = 1;

        protected override float DoCalculate(float parameter)
        {
            return CalculateCurve(parameter);
        }

        protected float CalculateCurve(float parameter)
        {
            var curveWrapper = new CurveWrapper()
            {
                Curve = animationCurve,
                TimeMultiplier = timeMultiplier,
                ValueMultiplier = valueMultiplier
            };
            return curveWrapper.Calculate(parameter);
        }
    }
}