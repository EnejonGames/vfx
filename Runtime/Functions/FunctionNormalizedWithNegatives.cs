﻿namespace Enejon.Vfx.Functions
{
    /// <summary>
    /// Takes a function that returns a normalized value (range[0,1]) and maps it to range[-1,1]
    /// where 0 -> -1 and 1 -> 1 
    /// </summary>
    public class FunctionNormalizedWithNegatives : Function
    {
        public Function function;
        protected override float DoCalculate(float parameter)
        {
            return (function.Calculate(parameter) - .5f) * 2;
        }
    }
}