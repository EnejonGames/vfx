using UnityEngine;

namespace Enejon.Vfx.Functions
{
    public class FunctionShifter : FunctionTrigger
    {
        public Function function;
        public float minimumDelta = 0f;
        public float maximumDelta = 1f;
        public float maximumValue = 1f;
        [SerializeField]
        private float _currentValue;
        protected override float DoCalculate(float parameter) 
        {
            return function.Calculate(parameter + _currentValue);
        }
        public override void Trigger() 
        {
            _currentValue += Random.Range(minimumDelta, maximumDelta);
            if (_currentValue >= maximumValue) _currentValue -= maximumValue;
        }

    }
}