using UnityEngine;
using System.Linq;

namespace Enejon.Vfx.Functions
{
    public class FunctionManipulator : FunctionCurve
    {
        public Function function;
        protected override float DoCalculate(float parameter)
        {
            return function.Calculate(parameter) * CalculateCurve(parameter);
        }
    }
}