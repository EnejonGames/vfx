using UnityEngine;

namespace Enejon.Vfx.Functions
{
    public class FunctionPair : MonoBehaviour
    {
        public Function firstFunction;
        public Function secondFunction;
    }
}