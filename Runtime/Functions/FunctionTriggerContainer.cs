using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enejon.Vfx.Functions
{
    public class FunctionTriggerContainer : MonoBehaviour
    {
        public enum Comparison 
        {
            GreaterThanResult, LessThanResult
        }
        public float triggerValue;
        public List<FunctionTrigger> triggerables; 
        public Comparison comparison;
        private bool _isTriggered = false;        
        public void TryTrigger(float result) 
        {
            if(CompareWithTriggerValue(result)) 
            {
                if (!_isTriggered) {
                    triggerables.ForEach(t => t.Trigger());
                    _isTriggered = true;
                }
            }
            else {
                _isTriggered = false;
            }
        }

        private bool CompareWithTriggerValue(float result) 
        {
            if (comparison == Comparison.GreaterThanResult) return triggerValue > result;
            else return triggerValue < result;
        }
    }
}