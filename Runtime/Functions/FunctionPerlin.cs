using UnityEngine;
using System.Linq;

namespace Enejon.Vfx.Functions
{
    public class FunctionPerlin : Function
    {
        [Range(0f, 1f)]
        public float multiplier = 1;
        protected override float DoCalculate(float parameter) {
            return Mathf.PerlinNoise(parameter * multiplier, 0f);
        }
    }
}