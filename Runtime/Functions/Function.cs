using UnityEngine;
using System.Collections.Generic;

namespace Enejon.Vfx.Functions
{
    public abstract class Function : MonoBehaviour
    {

        public List<FunctionTriggerContainer> subscriber = new List<FunctionTriggerContainer>();

        public float Calculate(float parameter) {
            float f = DoCalculate(parameter);
            subscriber.ForEach(s => s.TryTrigger(f));
            return f;
        }
        protected abstract float DoCalculate(float parameter);
    }
}