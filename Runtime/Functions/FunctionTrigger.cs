﻿namespace Enejon.Vfx.Functions
{
    public abstract class FunctionTrigger : Function
    {
        public abstract void Trigger();
    }
}