﻿using UnityEngine;

namespace Enejon.Vfx.LineRendererManipulation
{
    public interface ICurveWrapper
    {
        AnimationCurve Curve { get; set; }
        float ValueMultiplier { get; set; }
        float TimeMultiplier { get; set; }
        float Calculate(float parameter);
    }

    public class CurveWrapper : ICurveWrapper
    {
        public AnimationCurve Curve { get; set; }
        public float ValueMultiplier { get; set; }
        public float TimeMultiplier { get; set; }

        public float Calculate(float parameter)
        {
            return Curve.Evaluate(parameter * TimeMultiplier) * ValueMultiplier;
        }
    }
}