using UnityEngine;

namespace Enejon.Vfx.LineRendererManipulation
{
    public class Line : MonoBehaviour
    {
        public float density = 1;
        public Transform startPoint;
        public Transform endPoint;
        public int minimumPoints = 3;
        protected int NumberOfPoints => System.Math.Max((int)(density * Vector3.Distance(startPoint.position, endPoint.position)), minimumPoints);

        public virtual Vector3[] GetBasePositions() 
        {
            Vector3[] pointPositions = new Vector3[NumberOfPoints];
            for(int ii = 0; ii < NumberOfPoints; ii++)
            {
                pointPositions[ii] = startPoint.position + ((endPoint.position-startPoint.position)/(NumberOfPoints - 1)) * ii;
            }
            return pointPositions;
        }

    }
}