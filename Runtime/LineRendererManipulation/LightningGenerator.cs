using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Enejon.Vfx.LineRendererManipulation
{
    public class LightningGenerator : MonoBehaviour
    {

        public LineRendererManipulator prefab;
        public Transform startPoint;
        public Transform endPoint;
        public float density = 1;
        public int minimumPoints = 3;
        public int count = 5;
        public float timeMultiplier = 1f;
        public AnimationCurve weightTransition;
        public AnimationCurve widthTransition;
        public float weightMultiplier = 1f;
        public float widthMultiplier = 1f;
        public List<LineRendererManipulator> lrManipulators;

        public void TriggerDissolve() 
        {
            lrManipulators.ForEach(lrm => lrm.TriggerDissolve());
        }

        private void Awake() 
        {
            Generate();
            lrManipulators = GetComponentsInChildren<LineRendererManipulator>().ToList();            
        }

        private void Generate() 
        {
            for(int i = 1; i <= count; i++) GenerateSingleLightning(i);
        }

        private void GenerateSingleLightning(int iterator) {
            LineRendererManipulator instance = Instantiate(prefab, transform);
            Line line = instance.GetComponentInChildren<Line>();
            line.startPoint = startPoint;
            line.endPoint = endPoint;
            line.density = density;
            line.minimumPoints = minimumPoints;     

            instance.timeMultiplier = timeMultiplier;
            instance.timeOffset = Random.Range(0, 100f);
            instance.weight = weightTransition.Evaluate(GetEvaluateValue(iterator)) * weightMultiplier; 
            instance.weight *= iterator % 2 == 0 ? -1 : 1;
            instance.width = widthTransition.Evaluate(GetEvaluateValue(iterator)) * widthMultiplier;
            instance.Initialize();
        }        


        private float GetEvaluateValue(int iterator) 
        {
            return (float) iterator / count;
        }

    }
}