using System.Collections.Generic;
using UnityEngine;

namespace Enejon.Vfx.LineRendererManipulation
{
    public class Polygon
    {
        private int _nodeCount;
        private float _size;

        public Polygon(int nodeCount, float size)
        {
            _nodeCount = nodeCount;
            _size = size;

            Nodes = new List<Vector2>();
            for(int i = 0; i < nodeCount; i++)
            {
                float angle = (360f / nodeCount) * i;
                float radians = angle * Mathf.Deg2Rad;
                float x = Mathf.Cos(radians) * size;
                float y = Mathf.Sin(radians) * size;
                Nodes.Add(new Vector2(x, y));
            }

        }

        public List<Vector2> Nodes { get; private set; }
    }
}
