using System.Linq;
using UnityEngine;

namespace Enejon.Vfx.LineRendererManipulation
{
    public class LineCircle : Line
    {
        public override Vector3[] GetBasePositions() 
        {
            float radius = Vector3.Distance(startPoint.position, endPoint.position);
            float offsetX = startPoint.position.x;
            float offsetY = startPoint.position.y;
            Polygon polygon = new Polygon(NumberOfPoints, radius);            
            return polygon.Nodes.Select(n => new Vector3(n.x + offsetX, n.y + offsetY, startPoint.position.z)).ToArray();
        }        
    }
}