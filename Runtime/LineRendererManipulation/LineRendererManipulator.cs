using System;
using System.Collections.Generic;
using Enejon.Vfx.Functions;
using UnityEngine;

namespace Enejon.Vfx.LineRendererManipulation
{
    public class LineRendererManipulator : MonoBehaviour
    {
        public List<FunctionPair> functions;
        [Tooltip("Randomizes result positions (x,y)")]
        public FunctionPair randomizeFunctions;
        [Tooltip("Progression of the randomization weight (0: start of line, 1: end of line)")]
        public AnimationCurve randomizationProgression;
        public float weight = 1;
        public float timeMultiplier = 1;
        public float timeOffset = 0;
        public float width = 1;
        public AnimationCurve dissolveProgression;
        public float dissolveTime = 1f;
        private float _elapsedDissolveTime;
        private bool _isDissolving = false;
        private Line _line;
        private LineRenderer _lineRenderer;
        private float _elapsedTime;
        private Vector3[] _positions;
        private Vector2[] _freedomVectors;


        private void UpdatePositions() 
        {
            _positions = _line.GetBasePositions();
            _lineRenderer.positionCount = _positions.Length;
            _freedomVectors = GetFreedomVectors(_positions);
        }

        private void Awake() 
        {
            _lineRenderer = GetComponent<LineRenderer>();
            _line = GetComponent<Line>();
            Initialize();
        }        

        public void Initialize() 
        {
            _elapsedTime = timeOffset; 
        }

        public void TriggerDissolve() 
        {
            _isDissolving = true;
        }

        private void Dissolve() 
        {            
            _elapsedDissolveTime += Time.deltaTime;            
            float progression = Mathf.Min(_elapsedDissolveTime / dissolveTime, 1);
            _lineRenderer.widthMultiplier = width * dissolveProgression.Evaluate(progression);
        }

        private void Update() 
        {   
            _lineRenderer.widthMultiplier = width;
            if(_isDissolving) Dissolve();
            UpdatePositions();
            if(functions.Count == 0) return;
            _elapsedTime += Time.deltaTime * timeMultiplier;
            float[] functionResults = new float[_positions.Length];
            foreach(FunctionPair function in functions) 
            {
                float[] currentResults = CalculateFunctionResults(_positions.Length, function);
                functionResults = AddResults(currentResults, functionResults);                
            }
            var newPositions = CalculateAdjustedPositions(functionResults);
            if(randomizeFunctions != null) RandomizeResult(newPositions, functionResults);            
            _lineRenderer.SetPositions(newPositions);
        }

        private Vector2[] GetFreedomVectors(Vector3[] positions) 
        {
            Vector2[] freedomVectors = new Vector2[positions.Length];
            for(int ii = 0; ii < positions.Length; ii++) 
            {
                Vector3 currentPosition = positions[ii];
                Vector3 previousPosition = (ii - 1) < 0 ? currentPosition : positions[ii-1];
                Vector3 nextPosition = (ii + 1) >= positions.Length ? currentPosition : positions[ii+1];
                Vector3 firstLine = (currentPosition - previousPosition).normalized;
                Vector3 secondLine = (nextPosition - currentPosition).normalized;
                Vector2 combinedLine = new Vector2(firstLine.x + secondLine.x, firstLine.y + secondLine.y).normalized;
                freedomVectors[ii] = Vector2.Perpendicular(combinedLine);
            }
            return freedomVectors;
        }

        private float[] CalculateFunctionResults(int pointCount, FunctionPair function)
        {
            float[] results = new float[pointCount];
            for(int ii = 0; ii < pointCount; ii++)
            {
                results[ii] = function.firstFunction.Calculate(((float)ii / (pointCount - 1))) * 
                    function.secondFunction.Calculate(_elapsedTime);
            }
            return results;
        }

        private Vector3[] CalculateAdjustedPositions(float[] functionResults)
        {
            Vector3[] newPositions = new Vector3[_positions.Length];
            for(int ii = 0; ii < _positions.Length; ii++) 
            {
                Vector2 adjustment = _freedomVectors[ii] * (functionResults[ii] / functions.Count) * weight;
                newPositions[ii] = _positions[ii] + new Vector3(adjustment.x, adjustment.y, 0);
            }
            return newPositions;
        }

        private float[] AddResults(float[] array1, float[] array2) 
        {
            if(array1.Length != array2.Length) throw new ArgumentException(String.Format($"Can't call LineRendererManipulator.AddResults with arrays of varying lengths: array1: {0}, array2: {1}", array1.Length, array2.Length));
            float[] resultArray = new float[array1.Length];
            for(int ii = 0; ii < array1.Length; ii++) 
            {
                resultArray[ii] = array1[ii] + array2[ii];
            }
            return resultArray;
        }

        private void RandomizeResult(Vector3[] positions, float[] results) 
        {
            for(int ii = 0; ii < positions.Length; ii++) 
            {
                float randomizationWeight = GetRandomizationWeight((float) ii / (positions.Length - 1));
                positions[ii].x += randomizeFunctions.firstFunction.Calculate(_elapsedTime + (ii<<8)) * randomizationWeight;
                positions[ii].y += randomizeFunctions.secondFunction.Calculate(_elapsedTime + (ii<<8)) * randomizationWeight;
            }
        }

        private float GetRandomizationWeight(float normalizedParam) 
        {
            return randomizationProgression.Evaluate(normalizedParam) * weight;
        }
    }
}